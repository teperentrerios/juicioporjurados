<?php
ini_set('display_errors',false);
include('funciones/extract.php');
include('funciones/funcionesphp.php');
include('config.php');
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <title><?php echo $nombre_evento; ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="Implementación del sistema de sorteo de candidatos a integrar los jurados en los juicios por jurados de la provincia de Entre Ríos." name="description" />

<link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
<link rel="manifest" href="img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

	<script src="js/highcharts.js"></script>
	<script src="js/series-label.js"></script>
	<script src="js/exporting.js"></script>
	<script src="js/export-data.js"></script>
	<script src="js/accessibility.js"></script>	
	
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/venobox/venobox.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">

</head>

<body>

  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <a href="#intro" class="scrollto"><img src="img/logo.png" alt="" title=""></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Inicio</a></li>
          <li><a href="#about">Acerca</a></li>
<?php /*?>          <li><a href="#schedule">Programa</a></li>
<?php */?>          <li><a href="#ubicacion">En dónde</a></li>
          <li><a href="#resultados">Resultados</a></li>
          <li><a href="#supporters">Organismos</a></li>
          <li><a href="#contact">Contacto</a></li>
        </ul>
      </nav>
    </div>
  </header>

  <section id="intro">
    <div class="intro-container wow fadeIn">
      <h1 class="mb-4 pb-0">Sorteo<br>Juicio por Jurados</h1>
      <p class="mb-4 pb-0"><? echo $fecha_evento_larga; ?>, <? echo $lugar_evento; ?></p>
      <a href="https://www.youtube.com/watch?v=RT9evYJ1N6w" class="venobox play-btn mb-4" data-vbtype="video"
        data-autoplay="true"></a>
      <a href="#about" class="about-btn scrollto">Acerca del evento</a>
    </div>
  </section>

  <main id="main">
  
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h2>Acerca del evento</h2>
            <p style="text-align: justify;">El presidente del Superior Tribunal de Justicia (STJ), Emilio Castrillon, libró oficios al Poder Ejecutivo, Legislativo, Colegio de Abogados de la provincia, Asociación de la Magistratura y la Función Judicial, universidades y otras entidades, a fin de obtener los listados de las personas que por su condición funcional o desempeño profesional quedarían alcanzados por las incompatibilidades previstas en el artículo 14 de la Ley 10.746, que establece la creación del juicio por jurados.</p>
          </div>
          <div class="col-lg-3">
            <h3>Dónde</h3>
            <p><? echo $lugar_evento; echo "<br />"; echo $domicilio_evento; ?></p>
          </div>
          <div class="col-lg-3">
            <h3>Cuándo</h3>
            <p><? echo $dia_evento; ?><br><? echo $fecha_evento_larga; ?></p>
          </div>
        </div>
      </div>
    </section>
   
    <section id="ubicacion" class="wow fadeInUp">

      <div class="container-fluid">

        <div class="section-header">
         <p>&nbsp;</p>
         <p>&nbsp;</p>
          <h2>Ubicación</h2>
       </div>

        <div class="row no-gutters">
          <div class="col-lg-6 venue-map">
			  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3393.3247126132956!2d-60.52920308484405!3d-31.734336681299386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95b45278f5968d95%3A0x843511795c0e51ce!2sIAFAS!5e0!3m2!1ses-419!2sar!4v1591892876532!5m2!1ses-419!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			  
          </div>

          <div class="col-lg-6 venue-info">
            <div class="row justify-content-center">
              <div class="col-11 col-lg-8">
                <h3><? echo $lugar_evento; ?></h3>
                <p>En 1933 durante el gobierno del Dr. Luis Lorenzo Etchevehere se crea por Ley Provincial la "Caja de Asistencia Social y Sanitaria de la Provincia de Entre Ríos", para recaudar fondos para promover la acción social, casi nula hasta ese momento.<br />

La recaudación de esta caja se destinaría a combatir enfermedades, mejorar salas de hospitales, crear talleres, y colaborar con fundaciones que trabajen en la provincia, entre otras actividades.<br />

Por entonces, se da forma a la idea de una Lotería Provincial que realiza su primer sorteo el 24 de mayo de 1934, por medio de Lotería Nacional.<br />

Las actividades fueron creciendo y las necesidades sociales aumentando.<br />
Por ello, a fin de lograr una mejor funcionalidad y administración de los recursos financieros, se crea en 1972 el IAFAS (Instituto de Ayuda Financiera a la Acción Social).</p>
              </div>
            </div>
          </div>
        </div>

      </div>

    </section>
    
    <section id="resultados" class="section-with-bg wow fadeInUp">

      <div class="container">
        <div class="section-header">
			<p>&nbsp;</p>
			<p>&nbsp;</p>
          <h2>Resultados</h2>
        </div>
       <div class="row">
	<div class="col-lg-12">
		<?php include ('default.html')
		//include ('en-vivo.html'); ?>
        </div>
	</div>
      </div>

    </section>

    <section id="supporters" class="section-with-bg wow fadeInUp">

      <div class="container">
        <div class="section-header">
          <h2>Organismos</h2>
        </div>

        <div class="row no-gutters supporters-wrap clearfix">

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="img/supporters/1.png" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="img/supporters/2.png" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="img/supporters/3.png" class="img-fluid" alt="">
            </div>
          </div>
   
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="img/supporters/4.png" class="img-fluid" alt="">
            </div>
          </div>
        </div>

      </div>

    </section>

    <section id="faq" class="wow fadeInUp">

      <div class="container">

        <div class="section-header">
          <h2>Preguntas Frecuentes</h2>
        </div>

        <div class="row justify-content-center">
          <div class="col-lg-9">
              <ul id="faq-list">

                <li>
                  <a data-toggle="collapse" class="collapsed" href="#faq1">Requisitos para ser jurados <i class="fa fa-minus-circle"></i></a>
                  <div id="faq1" class="collapse" data-parent="#faq-list">
                    <p>
                      Podrán ser designados como jurados hombres y mujeres o los naturalizados que acrediten dos años de ejercicio previo de la ciudadanía argentina; quienes sean mayores de 18 años y tengan hasta 75; deberán saber leer, escribir, hablar y entender plenamente el idioma nacional; gozar del pleno ejercicio de los derechos políticos; tener domicilio conocido y una residencia no inferior a un año en el departamento de la jurisdicción del lugar del hecho.
                    </p>
                  </div>
                </li>
      
                <li>
                  <a data-toggle="collapse" href="#faq2" class="collapsed">Incompatibilidades <i class="fa fa-minus-circle"></i></a>
                  <div id="faq2" class="collapse" data-parent="#faq-list">
                    <p>
                     El artículo 14 de la Ley 10.746 establece las incompatibilidades y quienes no podrán desempeñarse como miembros de Jurados durante el tiempo que ejerzan sus funciones y hasta dos años posteriores a su cese.

Entre ellos figuran el gobernador, el vicegobernador, intendentes, viceintendentes, presidentes de comunas, ministros, secretarios, subsecretarios, directores y funcionarios equivalentes del poder Ejecutivo, de los municipios y de las comunas hasta el rango fuera de escalafón de los estatutos públicos. Senadores y diputados nacionales y provinciales, concejales, vocales de comunas y los funcionarios de los poderes legislativos nacional, provincial y municipal o comunal hasta el rango de fuera de escala. Los magistrados y funcionarios del Poder Judicial, del Ministerio Público Fiscal y del Ministerio Público de la Defensa. Tampoco podrán ser parte de jurados quienes ocupen cargos directivos de un partido político o sindicato legalmente reconocido, los integrantes de las fuerzas armadas y de seguridad en actividad, los ministros de cultos reconocidos oficialmente o no reconocidos; el fiscal de Estado, el contador General de la provincia, el tesorero General, los miembros directivos del Tribunal de Cuentas y cualquier otro funcionario de rango equivalente y sus análogos en el municipio y/o organismos públicos equivalentes de creación futura. Los abogados, procuradores, martilleros, escribanos y contadores públicos matriculados como así también los de profesiones afines al control de las partes. Los profesores universitarios de disciplinas jurídicas o de medicina legal o relacionados directamente con el Poder Judicial.
                    </p>
                  </div>
                </li>
      
              </ul>
          </div>
        </div>

      </div>

    </section>

    <section id="contact" class="section-bg wow fadeInUp">

      <div class="container">

        <div class="section-header">
          <h2>Contacto</h2>
          <p>Oficina de Juicio por Jurados</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Dirección</h3>
              <address><? echo $conf_direccion; ?>, <? echo $conf_ciudad; ?></address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Teléfono</h3>
              <p><a href="tel:<? echo $conf_telefono; ?>"><? echo $conf_telefono; ?></a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Correo Electrónico</h3>
              <p><a href="mailto:<? echo $conf_email; ?>"><? echo $conf_email; ?></a></p>
            </div>
          </div>

        </div>

      </div>
    </section>

  </main>

  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <img src="img/logo.png" alt="<?php echo $titulo_site;?>">
          </div>

          <div class="col-lg-3 col-md-6 footer-links">&nbsp;</div>
		 <div class="col-lg-3 col-md-6 footer-links">&nbsp;</div>
          
          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contacto</h4>
            <p><? echo $conf_direccion; ?><br>
              <? echo $conf_ciudad; ?>, CP <? echo $conf_cp;?><br>
              <strong>Teléfono:</strong> <? echo $conf_telefono; ?><br>
              <strong>Email:</strong> <? echo $conf_email; ?><br>
            </p>

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        Elaborado por Informática Tribunal Electoral de Entre Ríos
      </div>
    </div>

  </footer>

  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
	
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>

  <script src="js/main.js"></script>
</body>

</html>
