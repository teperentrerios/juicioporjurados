<?php

	$total_por_numero_f = 0;
	$total_por_numero_m = 0;

	foreach($array_resultados as $str_seccion =>$array_genero){

			$aux_seccion = explode('|',$str_seccion);
			$cod_seccion = $aux_seccion[0];
			$seccion = $aux_seccion[1];
			$cupo = $aux_seccion[2];
		
			//Axis: categories
			$cadena_categorias .= "'".utf8($seccion)."',";
		
		   //type: 'spline', name: 'Total por Sección',
			$cadena_spline_data .= ($cupo).',';

	
			foreach($array_genero as $genero =>$cantidad){
				
				if($genero == 'F'){
					//type: 'column', name: 'Femenino'
					$cadena_fem_data .= $cantidad.',';
					
   					//type: 'pie', name: 'Total por Género',    data: [{ name: 'Femenino', y:	
					$total_por_numero_f = $total_por_numero_f + $cantidad;

				}else{
					//type: 'column', name: 'Masculino'
					$cadena_mas_data .= $cantidad.',';
					
   					//type: 'pie', name: 'Total por Género',    data: [{ name: 'Masculino', y:	
					$total_por_numero_m = $total_por_numero_m + $cantidad;

					
				}
				
				
			}

	}



$categorias = substr($cadena_categorias,0,-1);
$cadena_spline_data = substr($cadena_spline_data,0,-1);
$cadena_fem_data = substr($cadena_fem_data,0,-1);
$cadena_mas_data = substr($cadena_mas_data,0,-1);


  //STRING PARA MOSTRAR EN EL TÍTULO DEL GRÁFICO
  $cadena_nros_sorteados = substr($cadena_nros_sorteados,0,-2);



$grafico = '<figure class="highcharts-figure">
    <div id="container" style="height: 35em;"></div>
</figure>';

$grafico .= "
<script>
Highcharts.chart('container', {
  title: {
    text: 'Precandidatos a Jurados por Sección y Género'
  },
  xAxis: {
    categories: [".$categorias."]
  },
  labels: {
    items: [{
      html: 'Por Género',
      style: {
        left: '50px',
        top: '18px',
        color: ( // theme
          Highcharts.defaultOptions.title.style &&
          Highcharts.defaultOptions.title.style.color
        ) || 'black'
      }
    }]
  },
  series: [{
    type: 'column',
    name: 'Femenino',
    data: [".$cadena_fem_data."]
  }, {
    type: 'column',
    name: 'Masculino',
    data: [".$cadena_mas_data."]
  },  {
    type: 'spline',
    name: 'Total por Sección',
    data: [".$cadena_spline_data."],
    marker: {
      lineWidth: 2,
      lineColor: Highcharts.getOptions().colors[2],
      fillColor: 'white'
    }
  }, {
    type: 'pie',
    name: 'Total por Género',
    data: [{
      name: 'Femenino',
      y: ".$total_por_numero_f.",
      color: Highcharts.getOptions().colors[0] // Color femenino
    }, {
      name: 'Masculino',
      y: ".$total_por_numero_m.",
      color: Highcharts.getOptions().colors[1] // Color masculino
    }],
    center: [100, 80],
    size: 100,
    showInLegend: false,
    dataLabels: {
      enabled: false
    }
  }]
});
</script>";

?>