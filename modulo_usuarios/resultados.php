<?php
include '../funciones/extract.php';
include '../modulo_usuarios/conexion.php';
include 'sesiones.php';
include 'sorteos.class.php';

$sorteos = new sorteos();

$numero = str_pad ($numero,3,"0",STR_PAD_LEFT);

if(strlen($numero) > 3){

    die ('El número ingresado es mayor a tres cifras');
   
}else{

//obtenemos los totales de candidatos ya sorteados y el total de cupos (la primera vez totales de candidatos  = 0)
$sql_cupos = $sorteos->sqlCupos();
$resultado_cupos = $conn->query($sql_cupos);

while($fila_cupos = $resultado_cupos->fetch_array()){
	
    if($fila_cupos['total_f'] < $fila_cupos['cupo_f']){
            $array_cupos['F'][$fila_cupos['codigo']] = $fila_cupos['total_f'].'|'.$fila_cupos['cupo_f'];
    }

    if($fila_cupos['total_m'] < $fila_cupos['cupo_m']){
            $array_cupos['M'][$fila_cupos['codigo']] = $fila_cupos['total_m'].'|'.$fila_cupos['cupo_m'];	
    }

}

//si el array_cupos viene vacío, es porque ya se agotaron todos los cupos!!
if(empty($array_cupos)){
	echo 'Ya está cubierto el cupo total por jurisdicción y por género<br>';
	include '../en-vivo.html';
}else{
		//iniciamos transaccion
		$conn->query('BEGIN');
		$bandera_transaccion = true;
			
		$sorteo_maestro = $sorteos->cargarNumero($numero);
		if(!$resultado = $conn->query($sorteo_maestro)){
			$bandera_transaccion = false;
			$mensaje_error .= 'Número ya cargado';
		}else{

			//obtenemos el id de la tabla sorteo_maestro -> para guardar el detalle
			$cod_sorteo = $conn->insert_id;
			
                                //Una vez obtenido el array de totales y cupos por género de cada sección armamos dos cadenas: una para cada sección por género, dichas cadenas serán las utilizadas para hacer el filtro del padrón 
				$cadena_departamentos_f = '';
				$cadena_departamentos_m = '';

				foreach($array_cupos as $genero => $array_seccion){
					foreach($array_seccion as $cod_seccion => $valores){
						if($genero == 'F'){
							$cadena_departamentos_f .= $cod_seccion.',';
						}else{
							$cadena_departamentos_m .= $cod_seccion.',';
						}
					}
				}

				//ahora que obtuvimos las dos cadenas se la pasamos como parámetros al sorteo_detalle (le quitamos la última coma)
				$cadena_departamentos_f = substr($cadena_departamentos_f,0,-1);
				$cadena_departamentos_m = substr($cadena_departamentos_m,0,-1);

			//grabamos el detalle con las cadenas recien armadas
			$sorteo_detalle = $sorteos->cargarDetalle($cod_sorteo, $numero, $cadena_departamentos_f, $cadena_departamentos_m);
			if(!$resultado_detalle = $conn->query($sorteo_detalle)){
				$bandera_transaccion = false;
				$mensaje_error .= 'Error al grabar precandidatos';
			}else{
				
				// si se llegó a un tope por sección y por género, hacemos el RANDOM para quitar el excedente
				include 'calcular_excedentes.php';	
				
				//mostrar resultados por seccion y género	
				$cadena_where = '';
				$sql_seccion_genero = $sorteos->sqlSeccionGenero($cadena_where);
				$resultado_seccion_genero = $conn->query($sql_seccion_genero);
				$cant_seccion_genero = $resultado_seccion_genero->num_rows;

				if($cant_seccion_genero == 0){
					$bandera_transaccion = false;
					$mensaje_error .= 'No hay ningún número sorteado<br>';
				}else{
					
					//obtenemos los numeros con los que se fue completando cada jurisdiccion
						$sql_numero_seccion = $sorteos->sqlNumeroSeccion();
						$resultado_numero_genero = $conn->query($sql_numero_seccion);
						while($fila_ng = $resultado_numero_genero->fetch_array()){
							$array_numero_seccion[$fila_ng['cod_seccion']][] = str_pad($fila_ng['numero'],3,"0",STR_PAD_LEFT);
						}		
				//	print_r($array_numero_seccion);
						include 'resultados_seccion_genero.php';
						include 'estadisticas_por_numero.php';
				}
			}
		}
	
		//finalizamos transacción
		if($bandera_transaccion == true){
                    $conn->query('COMMIT');

                    echo $barra_progreso.'<br>';
                    echo $nros_sorteados.'<br>';
                    echo $html.'<br>';
                    echo $grafico;                        

                    $fp = fopen('../en-vivo.html', 'w');
                    fwrite($fp, $barra_progreso);
                    fwrite($fp, $nros_sorteados);
                    fwrite($fp, $html);
                    fwrite($fp, $grafico);
                    fclose($fp);                        
                        
		}else{
                    $conn->query('ROLLBACK');
                    echo $mensaje_error;
		}
		
	}
	
	
}