 $(function(){
    $('#form').on('submit', function(e){
      e.preventDefault();
      cargarNumero(numero.value);
    });
});    

function cargarPagina(codigo){
	var parametros = {
		"pagina" : codigo
	};
	$.ajax({ 
            data:  parametros,
            url:   'modulo_usuarios/abmindex.php', 
            type:  'post',
            beforeSend: function () {
                    $("#cuerpo").html("<img src='img/ajax-loader2.gif'> Procesando, espere por favor...");
            },
            success:  function (response) {
                    $("#cuerpo").html(response);
                    $('html, body').animate({ scrollTop: $("#cuerpo").offset().top  }, 500);
            }
	})				
}

function cargarNumero(codigo) {
	
	var parametros = {
            "numero" : codigo
	};
	$.ajax({ 
            data:  parametros,
            url:   'modulo_usuarios/resultados.php', 
            type:  'post',
            beforeSend: function () {
                    $("#resultados").html("<img src='img/ajax-loader2.gif'> Procesando...");
            },
            success:  function (response) {
                    $("#resultados").html(response);
                    //$("#numero").val('').focus();
				 	$('html, body').animate({ scrollTop: $("#scroll").offset().top  }, 1000);

            }
	})
}

