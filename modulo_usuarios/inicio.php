<?php
include '../funciones/extract.php';
include 'sesiones.php';
?>

<p>
    <img src="img/logo2.png" class="img-responsive" />
</p>

<div class="row">
    <div class="col-sm-12" id="cuerpo">

        <form id="form" name="form" class="form-inline" method="post">
            <label for="numero"><input type="number" name="numero" id="numero" required="" class="form-control" style="width: 7em; font-size: 3em; text-align: center; font-family: fantasy" min="0" max="999" /></label>
            &nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-primary" name="enviarNumero" id="enviarNumero"><img src="img/btn-buscar.png" class="img-responsive"></button>
        </form>

        <div id="resultados"></div>

    </div>
</div>