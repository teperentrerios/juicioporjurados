<?php
include '../funciones/extract.php';
include '../funciones/funcionesphp.php';
include 'sesiones.php';


 while($fila = $resultado_seccion_genero->fetch_array()){
	 $array_resultados[$fila['cod_seccion'].'|'.$fila['seccion'].'|'.$fila['cupo']][$fila['genero']] = $fila['cantidad'];
	 
	 //guardamos el total encontrados por seccion y por género en la tabla secciones
	 $update_total_seccion_genero = $sorteos->setSeccionesResultados($fila['cod_seccion'], $fila['genero'], $fila['cantidad']);
	 //echo $update_total_seccion_genero.'<br>';
	 
	 if(!$resultado_update = $conn->query($update_total_seccion_genero)){
		 $bandera_transaccion = false;
		 $mensaje_error .= 'Error al actualizar valores encontrados en Sección '.$fila['seccion'].'<br>';
	 }

	 
 }


//ARMAMOS LA VARIABLE PARA MOSTRAR LUEGO POR PANTALLA
$html = '';
$html .= '
<div class="table-responsive">
	<h4>Resultados por Jurisdicción y Género</h4>
	<table class="table table-hover table-sm">
		<thead>
			<tr class="btn-primary">
				<th>Jurisdicción</th>
				<th style="text-align: right;">Femenino</th>
				<th style="text-align: right;">Masculino</th>
				<th style="text-align: center;">Cupo Jurisdicción</th>
				<th style="text-align: left;">Números Sorteados</th>
			</tr>
		</thead>
		<tbody>';
				$total_cupo = 0; 
				$total_por_numero_f = 0;
				$total_por_numero_m = 0;

			foreach($array_resultados as $str_seccion =>$array_genero){
				
					$aux_seccion = explode('|',$str_seccion);
					$cod_seccion = $aux_seccion[0];
					$seccion = $aux_seccion[1];
					$cupo = $aux_seccion[2];

			
		$html .= '		
			<tr>
				<td><strong>'.utf8($seccion).'</strong></td>';
				
				foreach($array_genero as $genero =>$cantidad){
					
					$total_cupo = $total_cupo + $cupo;
					
				
					$html .= '<td align="right">';
							if($cantidad < $cupo){
								$html .= '<font color="#FF0000"><strong>'.$cantidad.' / '.$cupo.'</strong></font>';
							}else{
								$html .= $cantidad.' / '.$cupo;
							}
					$html .= '	</td>';
					
							
					if($genero == 'F'){
						$total_por_numero_f = $total_por_numero_f + $cantidad;
					}else{
						$total_por_numero_m = $total_por_numero_m + $cantidad;
					}

			}
				
			$html .= ' <td align="center"><strong>'.($cupo*2).'</strong></td>';
			$html .= ' <td align="left"><span  class="text-muted">'.implode(', ',$array_numero_seccion[$cod_seccion]).'</span></td>
			</tr>';
			 } 
		$html .= '</tbody>
		<tfoot>
			<tr class="btn-primary">
				<td align="right"><strong>TOTALES</strong></td>
				<td align="right"><strong>'.imprimirmiles($total_por_numero_f).' / '.imprimirmiles($total_cupo/2).'</strong></td>
				<td align="right"><strong>'.imprimirmiles($total_por_numero_m).' / '.imprimirmiles($total_cupo/2).'</strong></td>
				<td align="center"><strong>'.imprimirmiles($total_cupo).'</strong></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
	</table>
</div>';

	 
	//guardamos el total encontrados por género para el número sorteado en la tabla sorteo_maestro
	 $update_total_numero_sorteado = $sorteos->setNumeroResultados($cod_sorteo, $total_por_numero_f, $total_por_numero_m);
	 if(!$resultado_update = $conn->query($update_total_numero_sorteado)){
		 $bandera_transaccion = false;
		 $mensaje_error .= 'Error al actualizar valores encontrados en Número '.$numero.'<br>';
	 }




//ARMAMOS LOS NÚMEROS SORTEADOS
	 $sql_sorteados = $sorteos->sqlNumerosSorteados();
	 $resultado_sorteados = $conn->query($sql_sorteados);

	  $nros_sorteados = '
	  
	  <h3 style="display:inline;" id="scroll"><strong>Nº Sorteados</strong></h3> ';
	  while($fila_sorteados = $resultado_sorteados->fetch_array()){
		  $nros_sorteados .= '<h2 style="display:inline;"><span class="badge badge-primary" style="margin:3px;">'.str_pad($fila_sorteados['numero'],3,'0',STR_PAD_LEFT).'</span></h2>';
	  }


//ARMAMOS LA BARRA DE PROGRESO
$total_encontrados = $total_por_numero_f + $total_por_numero_m;
$porcentaje_progreso = $sorteos->calcularPorcentajeProgreso($total_encontrados);

if($porcentaje_progreso == 100){
	$bar_class = 'bg-success';
}else{
	$bar_class = '';
}

$barra_progreso = '<div class="progress" style="height:3em; margin-bottom:20px;">
		<div class="progress-bar '.$bar_class.'" style="width:'.$porcentaje_progreso.'%"><strong><font size="+2">'.$porcentaje_progreso.'%</font></strong></div>
	 </div>';

?>