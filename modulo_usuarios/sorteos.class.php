<?php

class sorteos{
	
	function sqlCupos(){
            $sql = 'SELECT s.* FROM secciones s';
            return($sql);
	}
		
	function cargarNumero($numero){
            $sql = 'INSERT INTO sorteo_maestro (fecha_hora, numero) VALUES ("'.date('Y-m-d').' '.date('H:i:s').'", "'.$numero.'")';
            return($sql);
	}
	
	function cargarDetalle($cod_sorteo, $numero, $cadena_departamentos_f, $cadena_departamentos_m){
		
		//comprobamos que las cadenas tengan valor o no vengan vacías... si vienen vacias le asignamos un departamento fictio 19 para no romper el sql
		if(empty($cadena_departamentos_f)){
			$cadena_departamentos_f = 19;
		}
		if(empty($cadena_departamentos_m)){
			$cadena_departamentos_m = 19;
		}
		
		$sql = 'INSERT sorteo_detalle (cod_sorteo, matricula, clase, apellido, nombre, domicilio, tipo_ejemplar, seccion, genero, localidad, codpos) 
		SELECT CONCAT("'.$cod_sorteo.'"), p.matricula, p.clase, p.apellido, p.nombre, p.domicilio, p.tipo_ejemplar, p.seccion, p.genero, p.localidad, p.codpos
		FROM padron_definitivo_2019_generales p
		WHERE p.cod_jurado = 1 AND
		(p.genero = "F" AND p.seccion IN ('.$cadena_departamentos_f.') AND p.matricula LIKE "%'.$numero.'") OR (p.genero = "M" AND p.seccion IN ('.$cadena_departamentos_m.') AND p.matricula LIKE "%'.$numero.'");';
		
		return($sql);
	}
	
	function sqlCalcularExcedentesSeccionGenero(){
            $sql = 'SELECT s.`codigo` cod_seccion, s.descripcion seccion, sd.genero, 
                    COUNT(sd.codigo) cantidad, s.`cupo_f` cupo,
                    IF(COUNT(sd.codigo) > s.`cupo_f`, "S","N") bandera_excedente
                    FROM sorteo_maestro sm 
                    INNER JOIN sorteo_detalle sd ON (sd.cod_sorteo = sm.codigo) 
                    INNER JOIN secciones s ON (sd.seccion = s.codigo) 
                    LEFT JOIN `sorteo_excedentes` se ON (se.`cod_seccion` = s.`codigo` AND sd.`genero` = se.`genero`)
                    GROUP BY sd.seccion, sd.genero 
                    ORDER BY s.codigo, sd.codigo;';
            return($sql);
	}
		
	//seleccionamos en forma random la cantidad excedente del número sorteado
	function sqlRandom($numero, $cod_seccion, $genero, $cant_excendentes){
		$sql = 'SELECT sd.codigo FROM sorteo_detalle sd 
                        INNER JOIN sorteo_maestro sm ON (sd.cod_sorteo = sm.codigo)
                        WHERE sm.numero = '.$numero.' AND sd.seccion = '.$cod_seccion.' AND sd.genero = "'.$genero.'"
                        ORDER BY RAND()
                        LIMIT '.$cant_excendentes.';';
		return($sql);
	}
	
	//grabamos las personas que van a quedar fuera del sorteo por excende en el cupo en la sección y por género
	function setPersonasExcedentes($cadena_id_excedentes){
		$sql = 'UPDATE sorteo_detalle SET excedente = "S" WHERE codigo IN ('.$cadena_id_excedentes.')';
		return($sql);
	}
	
	//guardamos que sección, género ya se completó para un número
	function setExcedentesGenerales($cod_seccion, $genero, $cod_sorteo){
		global $conn;
		//obtenemos de la tabla sorteo_excedentes si ya está grabado el registro, si no está, hay que grabar para no ejecutar el random cada vez que sortea un numero para cuando ya fue calculado el excendete por sección y género
		$sql = 'SELECT * FROM `sorteo_excedentes` se WHERE se.`cod_seccion` = '.$cod_seccion.' AND se.`genero` = "'.$genero.'" ';
		$resultado = $conn->query($sql);
		$cant = $resultado->num_rows;
		
		if($cant == 0){
			$insert = 'INSERT INTO sorteo_excedentes (cod_seccion, genero, cod_sorteo) VALUES ('.$cod_seccion.', "'.$genero.'" , 
			'.$cod_sorteo.')';
			if(!$resultado = $conn->query($insert)){
				$bandera_transaccion = false;
				$mensaje_error .= 'Error al grabar datos generales para el cálculo de excedentes.<br>';
			}
			$calcular_random = true;
			return $calcular_random;
		}else{
			$calcular_random = false;
			return $calcular_random;
		}
	}
	
	//sql para mostrar los resultados	
	function sqlSeccionGenero($cadena_where){
		$sql = 'SELECT s.codigo cod_seccion, s.descripcion seccion, sd.genero, COUNT(sd.codigo) cantidad, s.cupo_f cupo
					FROM sorteo_maestro sm 
				INNER JOIN sorteo_detalle sd ON (sd.cod_sorteo = sm.codigo)
				INNER JOIN secciones s ON (sd.seccion = s.codigo)
				WHERE sd.excedente = "N" '.$cadena_where.'
				GROUP BY sd.seccion, sd.genero
				ORDER BY s.codigo, sd.genero';
	return($sql);
	}
	
	function setSeccionesResultados($cod_seccion, $genero, $total_genero){
		if($genero == 'F'){
			$cadena_update = 'total_f = '.$total_genero.'';
		}else{
			$cadena_update = 'total_m = '.$total_genero.'';
		}
		
		$sql = 'UPDATE secciones SET '.$cadena_update .' WHERE codigo = '.$cod_seccion.'';
		return($sql);
	}
	
	function setNumeroResultados($cod_sorteo, $total_por_numero_f, $total_por_numero_m){
		$sql = 'UPDATE sorteo_maestro SET total_f = '.$total_por_numero_f.', total_m = '.$total_por_numero_m.'	WHERE codigo = '.$cod_sorteo.'';
		return($sql);
	}
	
	function calcularPorcentajeProgreso($total_encontrados){
		global $conn;
		
		$sql_cupos = 'SELECT SUM(s.`cupo_f` + s.`cupo_m`) cupo FROM secciones s';
		$resultado_cupos = $conn->query($sql_cupos);
		$fila_cupos = $resultado_cupos->fetch_array();
		$cupos = $fila_cupos['cupo'];
		
		$porcentaje = floor(($total_encontrados*100)/$cupos);
		
		return($porcentaje);
	}
	
	function sqlNumerosSorteados(){
		$sql = 'SELECT * FROM sorteo_maestro sm ORDER BY sm.codigo';
		return($sql);
	}
	
	function sqlNumeroSeccion(){
		$sql = 'SELECT sm.codigo cod_sorteo, sm.numero, s.codigo cod_seccion, s.descripcion seccion, sd.genero, COUNT(sd.codigo) cantidad
					FROM sorteo_maestro sm 
					INNER JOIN sorteo_detalle sd ON (sd.cod_sorteo = sm.codigo) 
					INNER JOIN secciones s ON (sd.seccion = s.codigo)
					GROUP BY sm.numero, sd.seccion
					ORDER BY s.codigo, sd.codigo;';
		return($sql);
	}
}