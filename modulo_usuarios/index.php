<?php
include('../funciones/extract.php');
include('../funciones/funcionesphp.php');
include('../modulo_usuarios/conexion.php');
include('../config.php');
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base href="<? echo $conf_sitio; ?>/" />
    <title><? echo $titulo_site?></title>
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

		<script src="js/highcharts.js"></script>
		<script src="js/series-label.js"></script>
		<script src="js/exporting.js"></script>
		<script src="js/export-data.js"></script>
		<script src="js/accessibility.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
		<script src="lib/jquery/jquery.min.js"></script>
		<script src="modulo_usuarios/funciones.js"></script>            

</head>

  <body>
  
   <div class="container">
          
<?php
$cod_pagina = trim($_GET['codigo']); 

if(!empty($cod_pagina)){

	$pagina = explode('/',$cod_pagina);
	$cant_pagina = count($pagina);

	if($pagina[0] == 'sorteo-en-vivo'){
		include('../en-vivo.html');
	}else{	

			if($_SESSION['var_login_sorteo'] != true){
				if($pagina[1] == 'validar'){
					include 'validar.php';	
				}else{
					if($pagina[0] == 'usuarios'){
					include 'login.php';	
					}else{
						echo 'Error 404';
					}
				}

			}else{

				switch ($pagina[1]) {			

					//INICIO	
					case 'inicio':  include 'inicio.php'; break;										
					case 'reiniciar-sistema':  include 'reiniciar_sistema.php'; break;
				}
			}
	}
}
?>
	   </div>
	  
    </body>
        
</html>