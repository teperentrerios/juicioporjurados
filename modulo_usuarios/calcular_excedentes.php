<?php
include '../funciones/extract.php';
include 'sesiones.php';

    $sql_excedentes_seccion_genero = $sorteos->sqlCalcularExcedentesSeccionGenero();
    $resultado_excedentes_seccion_genero = $conn->query($sql_excedentes_seccion_genero);

    //echo $sql_excedentes_seccion_genero.'<br>';

    while($fila_exc = $resultado_excedentes_seccion_genero->fetch_array()){
        $array_excedentes[$fila_exc['bandera_excedente']][$fila_exc['cod_seccion']][$fila_exc['genero']]['cantidad'] = $fila_exc['cantidad'];
        $array_excedentes[$fila_exc['bandera_excedente']][$fila_exc['cod_seccion']][$fila_exc['genero']]['cupo'] =  $fila_exc['cupo'];
    }

    if(!empty($array_excedentes['S'])){

    foreach ($array_excedentes['S'] as $cod_seccion => $array_genero) {

        foreach ($array_genero as $genero => $valores) {
            $cantidad = $array_genero[$genero]['cantidad'];
            $cupo = $array_genero[$genero]['cupo'];

            //donde la cantidad de excedentes es la cantidad encontrada ($cantidad) menos el cupo ($cupo) // para cada jurisdicción por género
            $cant_excedentes = $cantidad - $cupo;

            //Ahora seteamos que jurisdicción y género ya llegaron al cupo para no volver a generar un random
            $calcular_random = $sorteos->setExcedentesGenerales($cod_seccion, $genero, $cod_sorteo);

            if ($calcular_random == true) {
                //ejecutamos el random
                $sql_random = $sorteos->sqlRandom($numero, $cod_seccion, $genero, $cant_excedentes);
                $resultado_random = $conn->query($sql_random);

                $cadena_id_excedentes = '';
                while ($fila_random = $resultado_random->fetch_array()) {
                    //Armamos con los códigos a setear como excedentes (sorteo_detalle.codigo)
                    $cadena_id_excedentes .= $fila_random['codigo'] . ',';
                }

                //limpiamos la última coma
                $cadena_id_excedentes = substr($cadena_id_excedentes, 0, -1);

                // y seteamos como excedentes sorteo_detalle.excedente = "S" a los que nos trajo el sql_random
                $set_personas_excedentes = $sorteos->setPersonasExcedentes($cadena_id_excedentes);
                if (!$resultado_personas_excedentes = $conn->query($set_personas_excedentes)) {
                    $bandera_transaccion = false;
                    $mensaje_error .= 'Error al grabar excedentes.' . $numero . ', ' . $cod_seccion . ', ' . $genero . ', ' . $cant_excedentes . '<br>';
                }
            }
        }
    }
}